import os
import sys
import textwrap
import subprocess
import time
from argparse import ArgumentParser

employee = textwrap.dedent(
    """
    class Employee:
        def __init__(self, name: str, salary: float) -> None:
            self.name_ = name
            self.salary_ = salary
    """
)

test_employee = textwrap.dedent(
    """
    from employee import Employee


    def test_employee():
        employee = Employee(name="person", salary=5000)
        assert employee.name_ == "person"
        assert employee.salary_ == 5000
    """
)

decorator_patch = textwrap.dedent(
    """
    diff --git a/employee.py b/employee.py
    index 581fa6b..b3d264b 100644
    --- a/employee.py
    +++ b/employee.py
    @@ -1,5 +1,13 @@

     class Employee:
         def __init__(self, name: str, salary: float) -> None:
    -        self.name_ = name
    -        self.salary_ = salary
    +        self.__name = name
    +        self.__salary = salary
    +
    +    @property
    +    def name(self):
    +        return self.__name
    +
    +    @property
    +    def salary(self):
    +        return self.__salary
    diff --git a/test_employee.py b/test_employee.py
    index adef316..ce9c679 100644
    --- a/test_employee.py
    +++ b/test_employee.py
    @@ -4,5 +4,5 @@ from employee import Employee

     def test_employee():
         employee = Employee(name="person", salary=5000)
    -    assert employee.name_ == "person"
    -    assert employee.salary_ == 5000
    +    assert employee.name == "person"
    +    assert employee.salary == 5000
    """
)


def run_subprocess(cmd: list):
    return subprocess.run(
        cmd, check=True, text=True, capture_output=True
    ).stdout


def get_git_version():
    out = run_subprocess(["git", "--version"])
    out = out.replace("git version", "")
    out = out.split('.')
    return [int(v) for v in out]


def init_repo():
    run_subprocess(["git", "init"])


def create_file(filename, content):
    with open(filename, "w") as new_file:
        new_file.write(content)


def commit_files(filenames, message):
    for filename in filenames:
        run_subprocess(["git", "add", filename])
    run_subprocess(["git", "commit", "-m", message])
    time.sleep(2)

def commit_file(filename, message):
    commit_files([filename], message)


def switch_to_new_branch(branch_name):
    v = get_git_version()
    if v[0] >= 2 and v[1] >= 23:
        run_subprocess(["git", "switch", "--create", branch_name])
    else:
        run_subprocess(["git", "checkout", "-b", branch_name])


def apply_patch(patch_content):
    i = 0
    patch_file = "patch.patch"
    while os.path.exists(patch_file):
        patch_file = f"patch_{i}.patch"
    with open(patch_file, "w") as patch:
        patch.write(patch_content)
    run_subprocess(["git", "apply", patch_file])
    os.remove(patch_file)


def get_all_modified():
    out = run_subprocess(["git", "ls-files", "-m"])
    return out.strip().strip('\n').split('\n')


if __name__ == "__main__":
    parser = ArgumentParser(description="Script to set up employee example")
    parser.add_argument("-f", "--project-folder",
                        required=False, default="MyProject",
                        help="Specify the folder name where to put the code")
    parser.add_argument("-b", "--create-decorator-branch",
                        required=False, action="store_true",
                        help="Apply patch to use the property decorator")
    args = vars(parser.parse_args())
    project_folder = args['project_folder']

    if os.path.exists(project_folder):
        sys.stderr.write(
            "ERROR: Project folder already exists!\n"
            "       Please remove folder or provide a different one using -f\n"
        )
        sys.exit(1)

    cwd = os.getcwd()
    os.makedirs(project_folder)
    os.chdir(project_folder)

    print("- start preparing employee example")
    print(f"-- created project folder '{project_folder}'")

    employee_file = "employee.py"
    test_employee_file = "test_employee.py"
    create_file(employee_file, employee)
    create_file(test_employee_file, test_employee)

    init_repo()
    commit_file(employee_file, "add employee class")
    commit_file(test_employee_file, "test employee class")
    print("-- made initial commits")

    if args["create_decorator_branch"]:
        branch_name = "feature/employee-use-property-decorator"
        switch_to_new_branch(branch_name)
        print(f"-- checked out a new branch {branch_name}")
        apply_patch(decorator_patch)
        print("-- applied decorator patch")
        commit_files(get_all_modified(), "[employee] use property decorator")
        print("-- committed patch changes")

    print(f"- finished preparing employee example in '{project_folder}'")

    os.chdir(cwd)
