import os
import subprocess
from time import sleep


def run(cmd: list):
    subprocess.run(cmd, check=True)


def touch(filename: str):
    with open(filename, "w") as new_file:
        new_file.write("")


def commit(message: str):
    new_file = f"{message}.txt"
    touch(new_file)
    run(["git", "add", new_file])
    sleep(2)  # ensure the time stamps of commits are distinguishable
    run(["git", "commit", "-m", message, new_file])


def switch_create(branch: str):
    run(["git", "switch", "--create", branch])


def switch(branch: str):
    run(["git", "switch", branch])


folder = "example"
os.makedirs(folder)
os.chdir(folder)
run(["git", "init"])

commit("A")

switch_create("feature")
commit("B")

switch("main")
commit("C")

switch("feature")
commit("D")
