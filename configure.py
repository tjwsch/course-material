import os
import sys
import shutil
import subprocess
from argparse import ArgumentParser
from textwrap import dedent

revjs_tags_url = "https://github.com/hakimel/reveal.js/archive/refs/tags"
revjs_version = "4.1.2"

highjs_style_url = "https://raw.githubusercontent.com/highlightjs/highlight.js/main/src/styles"
highlight_scheme_urls = [
    f"{highjs_style_url}/atom-one-light.css"
]


def remove_folder(folder: str) -> None:
    if os.path.exists(folder):
        shutil.rmtree(folder)


def remove_file(filename: str) -> None:
    if os.path.exists(filename):
        os.remove(filename)


def download_revealjs(version: str) -> None:
    print("-- getting reveal.js")
    zip_file = f"{version}.zip"
    folder_name = f"reveal.js-{version}"

    remove_file(zip_file)
    remove_folder(folder_name)
    print("  -- downloading zip archive")
    subprocess.run([
        "wget",
        "--no-check-certificate",
        f"{revjs_tags_url}/{zip_file}"
    ], check=True)

    print("  -- extracting zip archive")
    subprocess.run(["unzip", zip_file], check=True)

    print("  -- renaming extracted folder to 'reveal.js'")
    os.rename(folder_name, "reveal.js")


if __name__ == "__main__":
    parser = ArgumentParser(
        description="Prepares the environment for building the slides"
    )
    parser.add_argument(
        "-rv", "--revealjs-version",
        required=False, default=revjs_version,
        help="Specify reveal.js version to be used"
    )
    parser.add_argument(
        "-hs", "--highlight-scheme-urls",
        required=False, default=highlight_scheme_urls,
        nargs="*", help="Define syntax highlighting schemes to fetch"
    )
    args = vars(parser.parse_args())

    rjs_version = args["revealjs_version"]
    hscheme_urls = args["highlight_scheme_urls"]

    if os.getcwd() != os.path.dirname(os.path.abspath(__file__)):
        sys.stderr.write("Script must be run from the top folder of the course\n")
        sys.exit(1)

    if not os.path.exists("reveal.js"):
        download_revealjs(rjs_version)
        assert os.path.exists("reveal.js") and os.path.isdir("reveal.js")
    else:
        print(dedent("""
            WARNING: Skipped downloagin reveal.js because a folder named 'reveal.js'
                     already exists. That version may deviate from the one you specified.
        """))

    print("-- downloading specified syntax highlighting schemes")
    for scheme_url in hscheme_urls:
        print("--- {}".format(scheme_url))
        subprocess.run([
            "wget",
            "--no-check-certificate",
            scheme_url],
            check=True
        )

    print("-- moving highlighting schemes to reveal.js/plugin/highlight")
    for scheme_url in hscheme_urls:
        scheme_url = os.path.basename(scheme_url)
        target = os.path.join(
            os.path.join("reveal.js", "plugin"),
            "highlight"
        )
        print("  -- {}".format(scheme_url))
        shutil.move(scheme_url, target)
